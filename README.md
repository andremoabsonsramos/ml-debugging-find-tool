# Debugging Machine Learning: Finder Tool

## Uso

```
usage: java -jar finder.jar
 -createIndexes              clear the index folder and create new
    --indexDir <dir>         index dir
    --inputDir <dir>         input dir with repositories
    --keywords <json file>   json file with keywords to search
    --output1 <json file>    output file
    --output2 <json file>    output file
```

## Executando

```
java -jar finder.jar --inputDir /home/moabson/ml-debugging-find-tool/input --indexDir /home/moabson/ml-debugging-find-tool/indexes --keywords data/terms.json --output1 data/result.json --output2 data/files.json
```

## Resultado