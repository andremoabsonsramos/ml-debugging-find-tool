package br.ufal.ic.find;

import java.util.HashSet;
import java.util.Set;

public class KeywordInfo {
	
	String keyword;
	Integer hits;
	Integer countProjects;
	Set<String> projects;
	
	public KeywordInfo(String keyword, Integer count, Integer countProjects) {
		this.keyword = keyword;
		this.hits = count;
		this.countProjects = countProjects;
		this.projects = new HashSet<String>();
	}
	
	@Override
	public String toString() {
		return String.format("{%s %d %s}", keyword, hits, projects);
	}

}
