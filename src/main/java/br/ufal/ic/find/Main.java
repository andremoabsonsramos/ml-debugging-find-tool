package br.ufal.ic.find;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Main {

	final static Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) throws IOException, org.apache.lucene.queryparser.classic.ParseException {
		
		
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		
		try {
			PropertyConfigurator.configure("log4j.properties");
			
			Option inputDirOpt = Option.builder().longOpt("inputDir").argName("dir").hasArg().desc("input dir with repositories").build();
			Option indexDirOpt = Option.builder().longOpt("indexDir").argName("dir").hasArg().desc("index dir").build();
			Option keywordsOpt = Option.builder().longOpt("keywords").argName("file").hasArg().desc("json file with keywords to search").build();
			Option output1Opt = Option.builder().longOpt("output1").argName("file").hasArg().desc("output file").build();
			Option output2Opt = Option.builder().longOpt("output2").argName("file").hasArg().desc("output file").build();
			
			
			options.addOption(inputDirOpt);
			options.addOption(indexDirOpt);
			options.addOption(keywordsOpt);
			options.addOption(output1Opt);
			options.addOption(output2Opt);
			options.addOption("createIndexes", false, "clear the index folder and create new");
			
			CommandLine cmd = parser.parse( options, args);
			
			String inputDir = cmd.getOptionValue("inputDir");
			String indexDir = cmd.getOptionValue("indexDir");
			
			String termsJson = cmd.getOptionValue("keywords");
			String output1Json = cmd.getOptionValue("output1");
			String output2Json = cmd.getOptionValue("output2");
					
			Finder finder = new Finder(inputDir, indexDir);
			
			logger.info("Input directory: " + inputDir);
			logger.info("Index directory: " + indexDir);
			logger.info("Search Terms Json: " + termsJson); 
			logger.info("Output 1 Json: " + output1Json);
			logger.info("Output 2 Json: " + output2Json);
			
			if (cmd.hasOption("createIndexes")) {
				finder.createIndexes();
			}
			
			File terms = new File(termsJson);
			
			var result = finder.search(terms);
	
			logger.info("done");
			
			logger.info("saving json");
			
			try (Writer writer = new FileWriter(output1Json)) {
			    Gson gson = new GsonBuilder().create();
			    gson.toJson(result.getValue0().values(), writer);
			}
			
			try (Writer writer = new FileWriter(output2Json)) {
			    Gson gson = new GsonBuilder().create();
			    gson.toJson((Map<String, Map<String, List<String>>>) result.getValue1(), writer);
			}
			
			logger.info("done saving json");
		} catch (org.apache.commons.cli.ParseException e) {
            System.out.println(e.getMessage());
            new HelpFormatter().printHelp("java -jar finder.jar", options);
		} catch (Exception e) {
			throw e;
		}
		
	}

}
