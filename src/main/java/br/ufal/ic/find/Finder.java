package br.ufal.ic.find;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.javatuples.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Finder {

	private static final Logger logger = Logger.getLogger(Finder.class);

//	private final String INPUT_DIR = "/media/moabson/0C8AB694720CC881/ml-debugging-find-tool/input";
//	private final String INDEX_DIR = "/media/moabson/0C8AB694720CC881/ml-debugging-find-tool/indexes";
	
	private String inputDir = "";
	private String indexDir = "";

	public Finder(String inputDir, String indexDir) throws IOException {
		this.inputDir = inputDir;
		this.indexDir = indexDir;
	}

	public void createIndexes() throws IOException, IllegalStateException {
		File folder = new File(inputDir);

		if (!folder.isDirectory())
			throw new IllegalStateException("this not an folder");
		
		IndexWriter writer = createWriter();
		
		logger.info("deleting...");
		
		writer.deleteAll();
		
		logger.info("indexing...");

		visit(writer, folder);

		logger.info("done indexing...");

		writer.close();
	}

	public Pair<Map<String, KeywordInfo>, Map<String, Map<String, List<String>>>> search(File terms) throws ParseException, IOException {
		IndexSearcher searcher = createSearcher();

		List<String> termsToSearch = new ArrayList<String>();
		Map<String, Map<String, List<String>>> groupByProject = new HashMap<>();
		Map<String, KeywordInfo> infos = new HashMap<String, KeywordInfo>();

		Gson gson = new GsonBuilder().create();
		
		@SuppressWarnings("unchecked")
		Map<String, List<String>> languages = (Map<String, List<String>>) gson.fromJson(new FileReader(terms), Object.class);
		
		logger.info("terms: " + languages.get("py"));
		
		languages.get("py").forEach(term -> {
			termsToSearch.add(term);
		});

		termsToSearch.forEach(termText -> {
			try {
				TopDocs foundDocs = findContents(termText, searcher);

				logger.info("Total results for term " + termText + ": " + foundDocs.totalHits);

				for (ScoreDoc scoreDoc : foundDocs.scoreDocs) {
					Document doc = searcher.doc(scoreDoc.doc);
					// System.out.println("Path: "+ doc.get("path") + ", Score: " + scoreDoc.score);

					String docPath = doc.get("path");

//					String regex = "(\\/media\\/moabson\\/0C8AB694720CC881\\/ml-debugging-find-tool\\/input\\/)([\\w-_]+)(.*)";
					String regex = "(" + inputDir + "\\/)([\\w-_]+)(.*)";
					Pattern pattern = Pattern.compile(regex);

					Matcher matcher = pattern.matcher(docPath);
					
					if (matcher.matches()) {	
						String projectName = matcher.group(2);
						
						if (!infos.containsKey(termText)) {
							KeywordInfo info = new KeywordInfo(termText, 1, 1);
							info.projects.add(projectName);
							infos.put(termText, info);								
						} else {
							KeywordInfo info = infos.get(termText);
							info.hits = info.hits + 1;
							info.projects.add(projectName);
						}

						if (!groupByProject.containsKey(projectName)) {
							groupByProject.put(projectName, new HashMap<String, List<String>>());
						}

						Map<String, List<String>> project = groupByProject.get(projectName);
						
						if (!project.containsKey(termText)) {
							project.put(termText, new ArrayList<String>());
							project.get(termText).add(docPath);
						} else {
							project.get(termText).add(docPath);
						}
					}
				}
			} catch (Exception e) {
				logger.error(e);
			}
		});
		
		logger.info(groupByProject.keySet().size());

//		groupByProject.entrySet().forEach(project -> {
//			logger.info("Project: " + project.getKey());
//			project.getValue().entrySet().forEach(term -> {
//				logger.info("--> " + term.getKey() + " = " + term.getValue().size() + " files");
//				
//				term.getValue().forEach(filePath -> {
//					logger.info("----> " + filePath);
//				});
//			});
//		});
		
		infos.values().forEach(info -> {
			info.countProjects = info.projects.size();
		});

		return new Pair<Map<String, KeywordInfo>, Map<String, Map<String, List<String>>>>(infos, groupByProject);
	}

	public TopDocs findContents(String content, IndexSearcher searcher) throws ParseException, IOException {
		QueryParser queryParser = new QueryParser("contents", new StandardAnalyzer());
		Query query = queryParser.parse(content);

		TopDocs hits = searcher.search(query, searcher.count(query));

		return hits;
	}

	private IndexSearcher createSearcher() throws IOException {
		Directory dir = FSDirectory.open(Paths.get(indexDir));

		IndexReader reader = DirectoryReader.open(dir);
		IndexSearcher searcher = new IndexSearcher(reader);

		return searcher;

	}

	private void visit(IndexWriter writer, File current) throws IOException {
		if (current.isDirectory()) {
			File[] files = current.listFiles();

			for (File file : files) {
				visit(writer, file);
			}
		} else if (!current.isHidden() && current.exists() && current.canRead()) {
			try {
				String extesion = FilenameUtils.getExtension(current.getName());
				
				if (!extesion.isEmpty() && extesion.equalsIgnoreCase("py")) {					
					createDocument(writer, current);
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}

	private IndexWriter createWriter() throws IOException {
		FSDirectory dir = FSDirectory.open(Paths.get(indexDir));
		IndexWriterConfig conf = new IndexWriterConfig(new StandardAnalyzer());
		conf.setOpenMode(OpenMode.CREATE);
		IndexWriter writer = new IndexWriter(dir, conf);

		return writer;
	}

	private void createDocument(IndexWriter writer, File file) throws IOException {
		try (InputStream stream = new FileInputStream(file)) {
			Document doc = new Document();
			doc.add(new TextField("path", file.toString(), Store.YES));
			doc.add(new LongPoint("lastModified", file.lastModified()));
//            doc.add(new TextField("content", new String(Files.readAllBytes(Paths.get(file.toString()))), Store.YES));
			doc.add(new TextField("contents",
					new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));

			OpenMode mode = writer.getConfig().getOpenMode();

			if (mode == OpenMode.CREATE) {
				logger.debug("creating file " + file.toString());
				writer.addDocument(doc);
			} else {
				logger.debug("updating file " + file.toString());
				writer.updateDocument(new Term("path", file.toString()), doc);
			}
		}
	}
	

}
